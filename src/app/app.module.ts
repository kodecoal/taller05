import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Animations
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
//Material
import{ misComponentes } from './material';
import { DetalleTweetComponent } from './detalle-tweet/detalle-tweet.component';
import { ListadoTweesComponent } from './listado-twees/listado-twees.component';
import { TweetsService } from './tweets.service';

@NgModule({
  declarations: [
    AppComponent,
    DetalleTweetComponent,
    ListadoTweesComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    misComponentes
  ],
  providers: [TweetsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
