import { Component, OnInit } from '@angular/core';
import { Tweet } from '../model/tweet';
import { TweetsService } from '../tweets.service';
/*
1.crear el servicio ng generate service service/nombre
2.impportar el servicio y meterlo en el constructor como parametro


    const tweet1 = {
      id: 1,
      texto: 'You know something is unusual when your code runs perfect at first time',
      autor: 'JustADevGuy'
      };
      const tweet2 = {
      id: 1,
      texto: 'You know something is unusual when your code runs perfect at first time',
      autor: 'JustADevGuy'
      };
      const tweet3 = {
      id: 1,
      texto: '1969: What are you doing with that 2KB of RAM? -sending people to the moon',
      autor: 'AnotherDevGuy'
      };
      const tweet4 = {
      id: 1,
      texto: '2019: What are you doing with that 1.5GB of RAM? -Just checking facebook',
      autor: 'AnotherDevGuy'
      };
      this.misTweets.push(tweet1);
      this.misTweets.push(tweet2);
      this.misTweets.push(tweet3);
      this.misTweets.push(tweet4);




*/


@Component({
  selector: 'app-listado-twees',
  templateUrl: './listado-twees.component.html',
  styleUrls: ['./listado-twees.component.css']
})
export class ListadoTweesComponent implements OnInit {
  misTweets: Array<Tweet>;
  miTweet: Tweet;

  constructor(private tweetsService: TweetsService ) {

    this.miTweet = new Tweet();

    tweetsService.getAllTweets().subscribe(
      tweetobserba => {
        this.misTweets = tweetobserba;
      }
    )

  };
  ngOnInit() {
  }
  addNewTweet(){
    this.misTweets.push(this.miTweet);
  }

}
