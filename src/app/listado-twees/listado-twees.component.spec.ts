import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoTweesComponent } from './listado-twees.component';

describe('ListadoTweesComponent', () => {
  let component: ListadoTweesComponent;
  let fixture: ComponentFixture<ListadoTweesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoTweesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoTweesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
