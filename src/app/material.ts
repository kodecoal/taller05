import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { NgModule } from '@angular/core';

import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';


@NgModule({
  imports: [MatButtonModule, MatCheckboxModule,MatInputModule,MatCardModule,MatTableModule,MatToolbarModule,MatMenuModule,
    MatIconModule,MatBadgeModule,MatButtonToggleModule,MatBottomSheetModule,FormsModule, ReactiveFormsModule,MatGridListModule],
  exports: [MatButtonModule, MatCheckboxModule,MatInputModule,MatCardModule,MatTableModule,MatToolbarModule,MatMenuModule,
    MatIconModule,MatBadgeModule,MatButtonToggleModule,MatBottomSheetModule,FormsModule, ReactiveFormsModule,MatGridListModule],
})
export class misComponentes { }
