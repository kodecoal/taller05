import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Tweet } from './model/tweet';
import { ALL_TWEETS } from './dataTweet/dataTweet';

@Injectable({
  providedIn: 'root'
})
export class TweetsService {

  constructor() {

  }

  getAllTweets(): Observable<Tweet[]>
    {
      return of(ALL_TWEETS);

   }
}
